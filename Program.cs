﻿using System;
using System.Globalization;
namespace SomaEMedia
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Calcular calcular = new Calcular();

            calcular.Cabecalho();

            // Solicita ao usuario a quantidade de valores que ele quer somar;
            Console.WriteLine("Quantos valores você quer somar?");
            calcular.Entrada = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            calcular.ProcessamentoDosDados();

            calcular.Imprimir();
            
        }

        public class Calcular
        {
            public  double Entrada { get; set; }
            public double Soma { get; set; }
            public double Valor { get; set; }
            public double Media { get; set; }

            public string cabecalho { get; set; }

            public void ProcessamentoDosDados()
            {
                // Recebe um valor e o armazena, e retorna ao usuario uma mensagem para que digite outro valor;
                Soma = 0;
                for (int contador = 1; contador <= Entrada; contador++)
                {
                    Console.WriteLine("Digite um valor: ");
                    Valor = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                    Soma += Valor;
                }
                Media = Soma / Entrada;
            }

            public void Imprimir()
            {
                Console.Clear();

                // Imprime na tela a soma de todos os valores digitados pelo usuario e a média desses valores;
                Console.WriteLine($"A soma de todos os valores recebidos é: {Soma.ToString("F2", CultureInfo.InvariantCulture)}");
                Console.WriteLine("E a media é: " + Media.ToString("F2", CultureInfo.InvariantCulture));
                Console.ReadKey();
            }

            public void Cabecalho()
            {
                cabecalho = "SOMA & MÉDIA";
                Console.WriteLine($"\n********** {cabecalho} **********\n//////////////////////////////////");
            }
        }
        
    }
}
